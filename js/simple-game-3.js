/**
 * gameLoop zeichnet die Position des Players neu und synchronisiert
 * sich mit der Bildwiederholungsrate. Sie ruft sich selbst rekursiv auf.
 */
function gameLoop() {
	playerBounds = player.getBoundingClientRect();
	playingFieldBounds = playingField.getBoundingClientRect();
	/*
		Kollisionen abfragen
		Oberen/Unteren Rand des player mit dem oberen/unteren Rand des playingFields vergleichen,
		Richtung umkehren.
	*/
	if (playerBounds.bottom >= playingFieldBounds.bottom) {
		// Richtung umkehren
		velocityY *= -1;
		// Reibung anwenden
		velocityY *= friction;
		velocityX *= friction;
		playerY = playingFieldBounds.bottom - playerBounds.height - 1;
	} else if (playerBounds.top <= playingFieldBounds.top) {
		velocityY *= -1;
		velocityY *= friction;
		velocityX *= friction;
		playerY = playingFieldBounds.top + 1;
	}

	if (Math.abs(velocityY) <= 0.01) velocityY = 0;

	// in jedem Schritt gravity einwirken lassen
	velocityY += gravity;

	if (playerBounds.left <= playingFieldBounds.left) {
		velocityX *= -1;
		velocityX *= friction;
		velocityY *= friction;
		playerX = playingFieldBounds.left + 1;
	} else if (playerBounds.right >= playingFieldBounds.right) {
		velocityX *= -1;
		velocityX *= friction;
		velocityY *= friction;
		playerX = playingFieldBounds.right - playerBounds.width - 1;
	}
	if (Math.abs(velocityX) <= 0.01) velocityX = 0;

	playerX += window.scrollX + velocityX;
	playerY += window.scrollY + velocityY;

	player.style.setProperty("--x", playerX);
	player.style.setProperty("--y", playerY);

	window.requestAnimationFrame(gameLoop);
}

/**
 * Neue Position berechnen je nach gedrücktem Key
 * @param Event e
 */
function movePlayer(e) {
	// verhindern, dass scrolling stattfindet
	e.preventDefault();

	// Pfeil nach rechts abfragen und message ausgeben
	if (e.key === "ArrowRight") {
		velocityX += velocity;
	}
	else if (e.key === "ArrowLeft") {
		velocityX -= velocity;
	}

	if (e.key === "ArrowUp") {
		velocityY -= velocity;
	}
	else if (e.key === "ArrowDown") {
		velocityY += velocity;
	}
	else if (e.key === " ") {
		if (isJumping) return;
		velocityY += jumpVelocity;
		isJumping = true;
	}
	else {
		velocityY *= friction;
	}
}

const playingField = document.getElementById("playingField");
let playingFieldBounds = playingField.getBoundingClientRect();

// StartPosition
const player = document.getElementById("player");
let playerBounds = player.getBoundingClientRect();
let playerX = playerBounds.left;
let playerY = playerBounds.top;

// Beschleunigung
const velocity = 0.4;
let velocityX = 0;
let velocityY = 0;

const gravity = 0.06;
const friction = 0.9;
const jumpVelocity = -5;
let isJumping = false;

document.addEventListener("keydown", movePlayer);
document.addEventListener("keyup", (e) => {
	if (e.key === " ") {
		isJumping = false;
	}
});
// starte gameLoop
gameLoop();