/**
 * gameLoop zeichnet die Position des Players neu und synchronisiert
 * sich mit der Bildwiederholungsrate. Sie ruft sich selbst rekursiv auf.
 */
function gameLoop() {
	player.style.setProperty("--x", posX);
	player.style.setProperty("--y", posY);

	window.requestAnimationFrame(gameLoop);
}

/**
 * Neue Position berechnen je nach gedrücktem Key
 * @param Event e
 */
function movePlayer(e) {
	e.preventDefault();

	// Pfeil nach rechts abfragen und message ausgeben
	if (e.key === "ArrowRight") {
		posX = posX + window.scrollX + step;
	}
	else if (e.key === "ArrowLeft") {
		posX = posX + window.scrollX - step;
	}

	if (e.key === "ArrowUp") {
		posY = posY + window.scrollY - step;
	}
	else if (e.key === "ArrowDown") {
		posY = posY + window.scrollY + step;
	}
}

const player = document.getElementById("player");
const step = 3;
// Initialwerte meiner player Position
let bcr = player.getBoundingClientRect();
let posX = bcr.left;
let posY = bcr.top;

document.addEventListener("keydown", movePlayer);
// starte gameLoop
gameLoop();