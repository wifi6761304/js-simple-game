function movePlayer(e) {
	// aktuelle Position des player Objekts herausfinden und ausgeben
	const bcr = player.getBoundingClientRect();
	const currLeft = bcr.left;
	const currTop = bcr.top;
	console.log(bcr.top);

	// Pfeil nach rechts abfragen und message ausgeben
	// Alternativ e.keyCode 39
	if (e.key === "ArrowRight") {
		// Das Objekt um 2 Pixel nach rechts bewegen
		player.style.setProperty("--x", currLeft + step);
	}
	else if (e.key === "ArrowLeft") {
		player.style.setProperty("--x", currLeft - step);
	}


	if (e.key === "ArrowUp") {
		// Scrollen per Pfeil verhindern
		e.preventDefault();
		// Das Objekt um 2 Pixel nach oben/unten bewegen
		// Bei Scrollbalke scrollposition hinzufügen
		player.style.setProperty("--y", window.scrollY + currTop - step);
	}
	else if (e.key === "ArrowDown") {
		e.preventDefault();
		player.style.setProperty("--y", window.scrollY + currTop + step);
	}
}

const step = 2;
let posX;
let posY;
const player = document.getElementById("player");

document.addEventListener("keydown", movePlayer);